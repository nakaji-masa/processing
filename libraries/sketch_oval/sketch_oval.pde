import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT fft;
int fftSize;

void setup()
{
size(1024,480,P2D);
minim=new Minim(this);
player=minim.loadFile("MusMus-BGM-042.mp3");
player.loop();
}

void draw()
{
background(0);
fill(0, 127, 255, 255);

float x = player.left.level() * width; 
float y = player.right.level() * width;

ellipse(width/3, height/2,y,y);
ellipse(width/3*2,height/2,x,x);

}
